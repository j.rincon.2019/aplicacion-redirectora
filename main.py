import socket
import random

def process_request(clientsocket, addr):
    url = random.choice(urls)
    print(f"Esperando conexiones en el puerto {PORT}\r\n")
    recibido = clientsocket.recv(2048)
    print("Conexión establecida con:", addr)
    response = f"HTTP/1.1 302 Moved Temporarily\r\nLocation: {url}\r\n\r\n"
    print("Respuesta HTTP enviada al cliente:")
    print(response)
    clientsocket.sendall(response.encode('utf-8'))
    clientsocket.close()

if __name__ == '__main__':
    urls = ["https://www.aulavirtual.urjc.es/moodle/", "http://www.google.com/", "https://www.urjc.es/intranet-urjc"
            , "http://cursosweb.github.io/", "http://www.youtube.com/", "http://www.twitter.com/",
            "http://www.wikipedia.org/", "https://www.marca.com/", "https://www.nike.com/es/"]

    mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    PORT = 1234
    mysocket.bind(('localhost', PORT))
    mysocket.listen(5)

    try:
        while True:
            clientsocket, addr = mysocket.accept()
            process_request(clientsocket, addr)
    except KeyboardInterrupt:
        print("Saliendo...")
        mysocket.close()
